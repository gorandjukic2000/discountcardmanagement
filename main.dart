import 'dart:io';
import 'src/card.dart';

enum Category { cosmetics, books, accessories, services }
const List<int> Discount = [5, 10, 20, 30];

int createCardCode() {
  print("Choose card category:");
  for (Category category in Category.values) {
    print("${category.index + 1} - ${category.toString().substring(9)};");
  }

  int category = int.tryParse(stdin.readLineSync());
  if (category == null) {
    print("Input a number.\n");
    return 0;
  }
  if (category > Category.values.length || category <= 0) {
    print("Invalid category.\n");
    return 0;
  }

  int accumulation;
  print("Accumulation? (yes/no):");
  String accumulationInput = stdin.readLineSync();
  if (accumulationInput == "yes")
    accumulation = 1;
  else if (accumulationInput == "no")
    accumulation = 0;
  else {
    print("Wrong input.");
    return 0;
  }

  stdout.write("Choose discount (");
  for (int i = 0; i < Discount.length - 1; i++)
    stdout.write("${Discount[i]}, ");
  print("${Discount.last}):");

  int discount = int.tryParse(stdin.readLineSync());
  if (!Discount.contains(discount)) {
    print("Wrong input.");
    return 0;
  }

  DateTime now = DateTime.now();
  String date = now.day.toString().padLeft(2, "0") +
      now.month.toString().padLeft(2, "0") +
      (now.year + 1).toString().substring(2);

  int uid = (Card.cards.last.cardCode % 10000) + 1;

  String cardCode = category.toString() +
      accumulation.toString() +
      discount.toString().padLeft(2, "0") +
      date +
      uid.toString().padLeft(4, "0");

  return int.tryParse(cardCode);
}

void main() {
  List<Card> cards = Card.cards;
  cards.add(Card("Goxy", "Niš", 11052011210000));
  while (true) {
    print('''1. Read all cards
2. Read card details
3. Search a card by number
4. Create a new card
5. Update a card by its number
6. Delete a card by its number
7. Exit program''');
    int task = int.tryParse(stdin.readLineSync());
    switch (task) {
      case 1:
        print("");
        for (Card card in cards) {
          print("${card.cardCode} - ${card.fullName}\n");
        }
        break;

      case 2:
        print("Enter card number:");
        int cardCodeToRead = int.tryParse(stdin.readLineSync());
        bool found = false;

        for (Card card in cards) {
          if (card.cardCode == cardCodeToRead) {
            print(card.toString());
            found = true;
          }
        }

        if (!found) print("Card not found.\n");
        break;

      case 3:
        print("Enter a part of the card number (min 3 numbers):");
        String cardCodeToRead = stdin.readLineSync();
        if (cardCodeToRead.length < 3) {
          print("Less than 3 numbers.\n");
          break;
        }
        bool found = false;

        for (Card card in cards) {
          if (card.cardCode.toString().contains(cardCodeToRead)) {
            print(card.toString());
            found = true;
          }
        }

        if (!found) print("Card not found.\n");
        break;

      case 4:
        print("Enter your full name:");
        String name = stdin.readLineSync();

        print("Enter your city:");
        String city = stdin.readLineSync();

        int cardCode = createCardCode();
        if (cardCode == 0) break;

        Card newCard = Card(name, city, cardCode);
        cards.add(newCard);
        print(newCard.toString());
        break;

      case 5:
        print("Enter card number for updating:");
        int cardNumberForUpdating = int.tryParse(stdin.readLineSync());

        try {
          int indexOfCard = cards.indexOf(cards.firstWhere(
              (element) => element.cardCode == cardNumberForUpdating));

          print("Card found.\nHave you changed your name? (yes/no)");
          if (stdin.readLineSync() == "yes") {
            print("Enter your full name:");
            cards[indexOfCard].fullName = stdin.readLineSync();
          }

          print("Have you changed your location? (yes/no)");
          if (stdin.readLineSync() == "yes") {
            print("Enter your city:");
            cards[indexOfCard].customerCity = stdin.readLineSync();
          }

          int newCardCode = createCardCode();
          if (newCardCode == 0) break;

          cards[indexOfCard].cardCode = newCardCode;
          print(cards[indexOfCard].toString());
        } catch (ex) {
          print(ex);
        }
        break;

      case 6:
        print("Enter card number for deletion:");
        int cardNumberForDeletion = int.tryParse(stdin.readLineSync());
        bool found = false;

        for (Card card in cards) {
          if (card.cardCode == cardNumberForDeletion) {
            found = true;
            print("Card found. Are you sure you wanna delete it? (yes/no)");
            String confirmation = stdin.readLineSync();

            if (confirmation == "yes") {
              cards.remove(card);
              print("Card removed.");
              break;
            } else {
              print("Card not removed.");
              break;
            }
          }
        }

        if (!found) print("Card not found.");
        break;

      case 7:
        exit(0);
        break;

      default:
        print('Wrong input. Try again :)\n');
    }
  }
}
