class Card {
  String fullName;
  String customerCity;
  int cardCode;
  static List<Card> cards = List<Card>();

  Card(this.fullName, this.customerCity, this.cardCode);

  @override
  String toString() {
    return '''\nOwner: ${this.fullName}
City: ${this.customerCity}
Card code: ${this.cardCode}\n''';
  }
}
